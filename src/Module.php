<?php

namespace fafcms\settingmanager;

use fafcms\settingmanager\models\Setting;
use fafcms\helpers\abstractions\PluginModule;
use Yii;
use Closure;

/**
 * Class Module
 * @package fafcms\settingmanager
 */
class Module extends PluginModule
{
    public $controllerNamespace = 'fafcms\settingmanager\controllers';
    public $gridView = [];
    public $accessRules = [];
    private $errors = [];

    /**
     * @param string $code
     * @param mixed $default
     * @param bool $raw
     * @return mixed
     * @deprecated
     */
    public function getSetting(string $code, $default = null, bool $raw = true)
    {
        $cacheName = 'settingmanager_setting_value_'.$code.$raw;
        $value = Yii::$app->settingCache->get($cacheName);

        if ($value === false) {
            $setting = $this->findSetting('', $code, 0, 0, '');

            if ($setting === null) {
                $value = $default;
            } else {
                $value = Setting::getValue($setting, $raw);
                Yii::$app->settingCache->set($cacheName, $value, null);
            }
        }

        return $value;
    }

    /**
     * @param string $code
     * @param mixed $value
     * @return bool
     * @deprecated
     */
    public function setSetting(string $code, $value): bool
    {
        return $this->updateSettingValue('', $code, 0, 0, '', $value);
    }

    /**
     * @param string $module
     * @param string $code
     * @param int $projectId
     * @param int $languageId
     * @param string $variation
     * @param string $name
     * @param string|null $description
     * @param string $value_type
     * @param mixed $value
     * @return bool
     */
    public function createOrUpdateSetting(string $module, string $code, int $projectId, int $languageId, string $variation, string $name, ?string $description, string $value_type, $value): bool
    {
        $setting = $this->findSetting($module, $code, $projectId, $languageId, $variation);

        if ($setting === null) {
            return $this->createSetting($module, $code, $projectId, $languageId, $variation, $name, $description, $value_type, $value);
        }

        $this->setSettingMeta($setting, $name, $description, $value_type);
        return $this->updateSettingValue($module, $setting, $projectId, $languageId, $variation, $value);
    }

    /**
     * @param Setting $setting
     * @param string $name
     * @param string|null $description
     * @param string $value_type
     */
    private function setSettingMeta(Setting &$setting, string $name, ?string $description, string $value_type): void
    {
        $setting->setAttributes([
            'name' => $name,
            'description' => $description,
            'value_type' => $value_type
        ]);
    }

    /**
     * @param string $module
     * @param string $code
     * @param int $projectId
     * @param int $languageId
     * @param string $variation
     * @param string $name
     * @param string|null $description
     * @param string $value_type
     * @param mixed $value
     * @return bool
     */
    public function createSetting(string $module, string $code, int $projectId, int $languageId, string $variation, string $name, ?string $description, string $value_type, $value): bool
    {
        $setting = new Setting([
            'code' => $this->getFullSettingCode($module, $code, $projectId, $languageId, $variation),
        ]);

        $this->setSettingMeta($setting, $name, $description, $value_type);

        return $this->updateSettingValue($module, $setting, $projectId, $languageId, $variation, $value);
    }

    /**
     * @param string $module
     * @param $setting
     * @param int $projectId
     * @param int $languageId
     * @param string $variation
     * @param mixed $value
     * @return bool
     */
    public function updateSettingValue(string $module, $setting, int $projectId, int $languageId, string $variation, $value): bool
    {
        if (is_string($setting)) {
            $setting = $this->findSetting($module, $setting, $projectId, $languageId, $variation);
        }

        if ($setting->setCurrentValue($value) && $setting->save()) {
            $this->errors[$module][$setting->code] = [];
            return true;
        }

        $this->errors[$module][$setting->code] = $setting->getErrors();
        return false;
    }

    /**
     * @param string $module
     * @param string $code
     * @param int $projectId
     * @param int $languageId
     * @param string $variation
     * @param mixed $default
     * @return mixed
     */
    public function getSettingValue(string $module, string $code, int $projectId, int $languageId, string $variation, $default = null, $raw = true)
    {
        return Yii::$app->settingCache->getOrSet('settingmanager_setting_value_'.$this->getFullSettingCode($module, $code, $projectId, $languageId, $variation).$raw, function() use ($module, $code, $projectId, $languageId, $variation, $default, $raw) {
            $setting = $this->findSetting($module, $code, $projectId, $languageId, $variation);

            if ($setting === null) {
                if ($default instanceof Closure) {
                    return call_user_func($default, $module, $code, $projectId, $languageId, $variation, $raw);
                }

                return $default;
            }

            return Setting::getValue($setting, $raw);
        }, null);
    }

    /**
     * @param string $module
     * @param string $code
     * @return array|null
     */
    public function getErrors(string $module, string $code): ?array
    {
        return $this->errors[$module][$code]??null;
    }

    /**
     * @param string $module
     * @param string $code
     * @param int $projectId
     * @param int $languageId
     * @param string $variation
     * @return string
     */
    private function getFullSettingCode(string $module, string $code, int $projectId, int $languageId, string $variation): string
    {
        $fullCode = '';

        if ($module !== '') {
            $fullCode .= 'M_'.$module.'\\';
        }

        if ($projectId !== 0) {
            $fullCode .= 'P_'.$projectId.'\\';
        }

        if ($languageId !== 0) {
            $fullCode .= 'L_'.$languageId.'\\';
        }

        if ($variation !== '') {
            $fullCode .= 'V_'.$variation.'\\';
        }

        return $fullCode.$code;
    }

    /**
     * @param string $module
     * @param string $code
     * @param int $projectId
     * @param int $languageId
     * @param string $variation
     * @return Setting|null
     */
    private function findSetting(string $module, string $code, int $projectId, int $languageId, string $variation): ?Setting
    {
        return Setting::find()->where(['code' => $this->getFullSettingCode($module, $code, $projectId, $languageId, $variation)])->one();
    }
}
