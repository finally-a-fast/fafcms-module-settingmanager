<?php

namespace fafcms\settingmanager\migrations;

use yii\db\Migration;

/**
 * Class m190827_000200_init
 * @package fafcms\settingmanager\migrations
 */
class m190827_000200_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%setting}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'code'=> $this->string(255)->notNull(),
            'name'=> $this->string(255)->notNull(),
            'description'=> $this->text()->null()->defaultValue(null),
            'value_type'=> $this->string(255)->notNull(),
            'value_int'=> $this->integer(11)->null()->defaultValue(null),
            'value_varchar'=> $this->string(255)->null()->defaultValue(null),
            'value_text'=> $this->text()->null()->defaultValue(null),
            'value_bool'=> $this->tinyInteger(1)->null()->defaultValue(null),
            'value_model_class'=> $this->string(255)->null()->defaultValue(null),
            'value_model_id'=> $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-setting-created_by', '{{%setting}}', ['created_by'], false);
        $this->createIndex('idx-setting-updated_by', '{{%setting}}', ['updated_by'], false);
        $this->createIndex('idx-setting-deleted_by', '{{%setting}}', ['deleted_by'], false);

        $this->addForeignKey('fk-setting-created_by', '{{%setting}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-setting-updated_by', '{{%setting}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-setting-deleted_by', '{{%setting}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-setting-created_by', '{{%setting}}');
        $this->dropForeignKey('fk-setting-updated_by', '{{%setting}}');
        $this->dropForeignKey('fk-setting-deleted_by', '{{%setting}}');

        $this->dropTable('{{%setting}}');
    }
}
