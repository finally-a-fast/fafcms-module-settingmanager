<?php

namespace fafcms\settingmanager\migrations;

use fafcms\settingmanager\models\Setting;
use yii\db\Migration;

/**
 * Class m190827_000200_init
 * @package fafcms\settingmanager\migrations
 */
class m200120_205957_prefix extends Migration
{
    public function safeUp()
    {
        $this->renameTable('{{%setting}}', Setting::tableName());
    }

    public function safeDown()
    {
        $this->renameTable(Setting::tableName(), '{{%setting}}');
    }
}
