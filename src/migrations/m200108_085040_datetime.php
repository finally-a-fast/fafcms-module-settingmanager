<?php

namespace fafcms\settingmanager\migrations;

use yii\db\Migration;

/**
 * Class m200108_085040_datetime
 * @package fafcms\settingmanager\migrations
 */
class m200108_085040_datetime extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%setting}}', 'value_date', $this->date()->null()->defaultValue(null)->after('value_model_id'));
        $this->addColumn('{{%setting}}', 'value_time', $this->time()->null()->defaultValue(null)->after('value_date'));
        $this->addColumn('{{%setting}}', 'value_datetime', $this->dateTime()->null()->defaultValue(null)->after('value_time'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%setting}}', 'value_date');
        $this->dropColumn('{{%setting}}', 'value_time');
        $this->dropColumn('{{%setting}}', 'value_datetime');
    }
}
