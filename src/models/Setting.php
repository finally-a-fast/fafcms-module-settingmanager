<?php

namespace fafcms\settingmanager\models;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\models\User;
use fafcms\helpers\ActiveRecord;
use fafcms\settingmanager\Bootstrap;
use Yii;
use DateTime;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use fafcms\fafcms\components\ViewComponent;
use fafcms\settingmanager\Module;

/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $status
 * @property string $code
 * @property string $name
 * @property string|null $description
 * @property string $value_type
 * @property int|null $value_int
 * @property string|null $value_varchar
 * @property string|null $value_text
 * @property int|null $value_bool
 * @property string|null $value_model_class
 * @property int|null $value_model_id
 * @property string|null $value_date
 * @property string|null $value_time
 * @property string|null $value_datetime
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property User $createdBy
 * @property User $deletedBy
 * @property User $updatedBy
 */
class Setting extends ActiveRecord
{
    public function getEditData()
    {
        return [
            'url' => Bootstrap::$id.'/setting',
            'icon' => 'cog-outline',
            'plural' => Yii::t('fafcms-settingmanager', 'Settings'),
            'singular' => Yii::t('fafcms-settingmanager', 'Setting')
        ];
    }

    public function getOptions($attribute)
    {
        if ($attribute === 'value_model_class') {
            $modelClasses = [];

            foreach (Yii::$app->fafcms->getModelClasses() as $model) {
                $modelClassName = $model['fullyQualifiedClassName'];

                if (!class_exists($modelClassName)) {
                    continue;
                }

                try {
                    if (($modelClassName::instance()->editDataSingular ?? null) !== null) {
                        $modelClasses[$modelClassName] = $modelClassName::instance()->editDataSingular;
                    } else {
                        $modelClasses[$modelClassName] = $modelClassName;
                    }
                } catch (\Exception $e) {
                    $modelClasses[$modelClassName] = $modelClassName;
                }
            }

            return ['' => ''] + $modelClasses;
        }

        if ($attribute === 'value_model_id' && $this->value_model_class !== null && isset(Yii::$app->fafcms->getModelClasses()[$this->value_model_class])) {
            if (method_exists($this->value_model_class, 'getOptions')) {
                return ['' => ''] + $this->value_model_class::getOptions(false);
            }

            return ['' => ''] + ArrayHelper::map($this->value_model_class::find()->select('id')->asArray()->all(), 'id', 'id');
        }

        $options = [
            'value_type' => [
                FafcmsComponent::VALUE_TYPE_INT => Yii::t('fafcms-settingmanager', 'Integer'),
                FafcmsComponent::VALUE_TYPE_VARCHAR => Yii::t('fafcms-settingmanager', 'Varchar'),
                FafcmsComponent::VALUE_TYPE_TEXT => Yii::t('fafcms-settingmanager', 'Text'),
                FafcmsComponent::VALUE_TYPE_BOOL => Yii::t('fafcms-settingmanager', 'Boolean'),
                FafcmsComponent::VALUE_TYPE_MODEL => Yii::t('fafcms-settingmanager', 'Model'),
                FafcmsComponent::VALUE_TYPE_DATE => Yii::t('fafcms-settingmanager', 'Date'),
                FafcmsComponent::VALUE_TYPE_TIME => Yii::t('fafcms-settingmanager', 'Time'),
                FafcmsComponent::VALUE_TYPE_DATETIME => Yii::t('fafcms-settingmanager', 'Date and time'),
            ]
        ];

        return $options[$attribute] ?? [];
    }

    public static function getValue($setting, $raw = false)
    {
        $value = null;

        switch ($setting['value_type']) {
            case FafcmsComponent::VALUE_TYPE_INT:
                $value = $setting['value_int'];
                break;
            case FafcmsComponent::VALUE_TYPE_VARCHAR:
                $value = $setting['value_varchar'];
                break;
            case FafcmsComponent::VALUE_TYPE_TEXT:
                $value = $setting['value_text'];
                break;
            case FafcmsComponent::VALUE_TYPE_BOOL:
                $value = $setting['value_bool'];
                break;
            case FafcmsComponent::VALUE_TYPE_MODEL:
                $value = $setting['value_model_id'];

                if (!$raw &&
                    $setting['value_model_class'] !== null &&
                    isset(Yii::$app->fafcms->getModelClasses()[$setting['value_model_class']])
                ) {
                    if (method_exists($setting['value_model_class'], 'getEditDataSingular')) {
                        $value = $setting['value_model_class']::instance()->getEditDataSingular().': ';
                    } else {
                        $value = $setting['value_model_class'].': ';
                    }

                    if (method_exists($setting['value_model_class'], 'getExtendedLabel')) {
                        $model = $setting['value_model_class']::find()->where(['id' => $setting['value_model_id']])->one();

                        if ($model !== null) {
                            $label = $model->getExtendedLabel();
                        }
                    } elseif (method_exists($setting['value_model_class'], 'getOptions')) {
                        $label = $setting['value_model_class']::getOptions()[$setting['value_model_id']];
                    }

                    $value .= $label ?? $setting['value_model_id'];
                }
                break;
            case FafcmsComponent::VALUE_TYPE_DATE:
                $value = $setting['value_date'];

                if ($raw) {
                    $value = DateTime::createFromFormat('Y-m-d', $value);
                } else {
                    $value = Yii::$app->formatter->asDate($value);
                }
                break;
            case FafcmsComponent::VALUE_TYPE_TIME:
                $value = $setting['value_datetime'];

                if ($raw) {
                    $value = DateTime::createFromFormat('H:i:s', $value);
                } else {
                    $value = Yii::$app->formatter->asTime($value);
                }
                break;
            case FafcmsComponent::VALUE_TYPE_DATETIME:
                $value = $setting['value_datetime'];

                if ($raw) {
                    $value = DateTime::createFromFormat('Y-m-d H:i:s', $value);
                } else {
                    $value = Yii::$app->formatter->asDatetime($value);
                }
                break;
        }

        return $value;
    }

    public function getCurrentValue($raw = false)
    {
        return self::getValue($this, $raw);
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function setCurrentValue($value): bool
    {
        switch ($this->value_type) {
            case FafcmsComponent::VALUE_TYPE_INT:
                $this->value_int = $value;
                break;
            case FafcmsComponent::VALUE_TYPE_VARCHAR:
                $this->value_varchar = $value;
                break;
            case FafcmsComponent::VALUE_TYPE_TEXT:
                $this->value_text = $value;
                break;
            case FafcmsComponent::VALUE_TYPE_BOOL:
                $this->value_bool = $value;
                break;
            case FafcmsComponent::VALUE_TYPE_TIME:
            case FafcmsComponent::VALUE_TYPE_DATE:
            case FafcmsComponent::VALUE_TYPE_DATETIME:
                if (is_int($value)) {
                    $value = DateTime::createFromFormat( 'U', $value);
                } elseif (!($value instanceof DateTime)) {
                    $value = date_parse($value);
                    $value = $value['year'].'-'.$value['month'].'-'.$value['day'].' '.$value['hour'].':'.($value['minute'] < 10?'0':'').$value['minute'].':'.($value['second'] < 10?'0':'').$value['second'];
                    $value = DateTime::createFromFormat('Y-n-j G:i:s', $value);
                }

                if ($value === false) {
                    return false;
                }

                if ($this->value_type === FafcmsComponent::VALUE_TYPE_TIME) {
                    $this->value_time = $value->format('H:i:s');
                } elseif ($this->value_type === FafcmsComponent::VALUE_TYPE_DATE) {
                    $this->value_date = $value->format('Y-m-d');
                } else {
                    $this->value_datetime = $value->format('Y-m-d H:i:s');
                }
                break;
            case FafcmsComponent::VALUE_TYPE_MODEL:
                if (is_array($value)) {
                    if (!isset(Yii::$app->fafcms->getModelClasses()[$value['model']])) {
                        return false;
                    }

                    $this->value_model_id = $value['id'];
                    $this->value_model_class = $value['model'];
                } else {
                    $this->value_model_id = $value;
                }
                break;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Yii::$app->settingCache->delete('settingmanager_setting_value_'.$this->code.'0');
        Yii::$app->settingCache->delete('settingmanager_setting_value_'.$this->code.'1');
        //Yii::$app->settingCache->flush();
    }

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%setting}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        $js = 'function (typeAttribute, typeValue) {
            var $form = typeAttribute.$form

            var inputIds = {
                \'int\': [$form.yiiActiveForm(\'find\', \'setting-value_int\')],
                \'varchar\': [$form.yiiActiveForm(\'find\', \'setting-value_varchar\')],
                \'text\': [$form.yiiActiveForm(\'find\', \'setting-value_text\')],
                \'bool\': [$form.yiiActiveForm(\'find\', \'setting-value_bool\')],
                \'model\': [$form.yiiActiveForm(\'find\', \'setting-value_model_class\'), $form.yiiActiveForm(\'find\', \'setting-value_model_id\')],
                \'date\': [$form.yiiActiveForm(\'find\', \'setting-value_date\')],
                \'time\': [$form.yiiActiveForm(\'find\', \'setting-value_time\')],
                \'datetime\': [$form.yiiActiveForm(\'find\', \'setting-value_datetime\')],
            }

            $.each(inputIds, function(i, attributeGroup) {
                $.each(attributeGroup, function(i, attribute) {
                    $(attribute.container).addClass(\'hide\')
                })
            })

            $.each(inputIds[typeValue], function(i, attribute) {
                $(attribute.container).removeClass(\'hide\')
            })

            return true
        }';

        Yii::$app->getView()->registerJs('
            window.setTimeout(function() {
                var $form = $(\'#'.strtolower($this->formName()).'-form\')
                var typeAttribute = $form.yiiActiveForm(\'find\', \'setting-value_type\')
                typeAttribute.$form = $form;
                ('.$js.')(typeAttribute, typeAttribute.value);
            }, 0);
        ',
                                         ViewComponent::POS_READY);

        return [
            [['code', 'name'], 'required'],
            [['value_type'], 'required', 'whenClient' => $js],
            [['value_int'], 'required', 'skipOnEmpty' => true, 'when' => function () {
                return $this->value_type === 'int';
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'setting-value_type\').value === \'int\'
            }'],
            [['value_varchar'], 'required', 'skipOnEmpty' => true, 'when' => function () {
                return $this->value_type === 'varchar';
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'setting-value_type\').value === \'varchar\'
            }'],
            [['value_text'], 'required', 'skipOnEmpty' => true, 'when' => function () {
                return $this->value_type === 'text';
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'setting-value_type\').value === \'text\'
            }'],
            [['value_bool'], 'required', 'skipOnEmpty' => true, 'when' => function () {
                return $this->value_type === 'bool';
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'setting-value_type\').value === \'bool\'
            }'],
            [['value_model_class', 'value_model_id'], 'required', 'skipOnEmpty' => true, 'when' => function () {
                return $this->value_type === 'model';
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'setting-value_type\').value === \'model\'
            }'],
            [['value_date'], 'required', 'skipOnEmpty' => true, 'when' => function () {
                return $this->value_type === 'date';
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'setting-value_type\').value === \'date\'
            }'],
            [['value_time'], 'required', 'skipOnEmpty' => true, 'when' => function () {
                return $this->value_type === 'time';
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'setting-value_type\').value === \'time\'
            }'],
            [['value_datetime'], 'required', 'skipOnEmpty' => true, 'when' => function () {
                return $this->value_type === 'datetime';
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'setting-value_type\').value === \'datetime\'
            }'],
            [['description', 'value_text'], 'string'],
            [['value_bool'], 'boolean'],
            [['value_int', 'value_model_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['value_date', 'value_time', 'value_datetime', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['status', 'code', 'name', 'value_type', 'value_varchar', 'value_model_class'], 'string', 'max' => 255],
            [['code'], 'unique'],
            [['value_model_class'], 'in', 'range' => array_keys($this->getOptions('value_model_class'))],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('fafcms-settingmanager', 'ID'),
            'status' => Yii::t('fafcms-settingmanager', 'Status'),
            'code' => Yii::t('fafcms-settingmanager', 'Code'),
            'name' => Yii::t('fafcms-settingmanager', 'Name'),
            'description' => Yii::t('fafcms-settingmanager', 'Description'),
            'value_type' => Yii::t('fafcms-settingmanager', 'Value Type'),
            'value_int' => Yii::t('fafcms-settingmanager', 'Value Int'),
            'value_varchar' => Yii::t('fafcms-settingmanager', 'Value Varchar'),
            'value_text' => Yii::t('fafcms-settingmanager', 'Value Text'),
            'value_bool' => Yii::t('fafcms-settingmanager', 'Value Boolean'),
            'value_model_class' => Yii::t('fafcms-settingmanager', 'Value Model Id'),
            'value_model_id' => Yii::t('fafcms-settingmanager', 'Value Model Class'),
            'value_date' => Yii::t('fafcms-settingmanager', 'Value Date'),
            'value_time' => Yii::t('fafcms-settingmanager', 'Value Time'),
            'value_datetime' => Yii::t('fafcms-settingmanager', 'Value Datetime'),
            'created_by' => Yii::t('fafcms-settingmanager', 'Created By'),
            'updated_by' => Yii::t('fafcms-settingmanager', 'Updated By'),
            'deleted_by' => Yii::t('fafcms-settingmanager', 'Deleted By'),
            'created_at' => Yii::t('fafcms-settingmanager', 'Created At'),
            'updated_at' => Yii::t('fafcms-settingmanager', 'Updated At'),
            'deleted_at' => Yii::t('fafcms-settingmanager', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'deleted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
