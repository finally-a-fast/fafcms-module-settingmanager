<?php
namespace fafcms\settingmanager\assets;

use yii\web\AssetBundle;

class SettingmanagerAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/settingmanager';

    public $js = [
        //'js/settingmanager.js',
    ];

    public $css = [
        //'css/settingmanager'.(YII_ENV_DEV?'':'.min').'.css',
    ];

    public $depends = [
    ];
}
