<?php

namespace fafcms\settingmanager;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\sitemanager\assets\SitemanagerAsset;
use yii\base\Application;
use Yii;
use yii\helpers\Json;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;
use fafcms\settingmanager\models\Setting;
use fafcms\settingmanager\assets\SettingmanagerAsset;

class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-settingmanager';
    public static $tablePrefix = 'fafcms-sett_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-settingmanager'])) {
            $app->i18n->translations['fafcms-settingmanager'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ .'/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        $module->accessRules = array_merge([
            'setting' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
        ], $module->accessRules);

        Yii::$app->view->addNavigationItems(
            FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
            [
                'system' => [
                    'items' => [
                        'settings'    => [
                            'items' => [
                                'settings' => [
                                    'after' => 'installer',
                                    'label' => Setting::instance()->getEditData()['plural'],
                                    'icon' => Setting::instance()->getEditData()['icon'],
                                    'visible' => true,
                                    'url' => ['/'.Setting::instance()->getEditData()['url']]
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:\d+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
        ]);

        return true;
    }

    protected function bootstrapBackendApp(Application $app, PluginModule $module): bool
    {
        SettingmanagerAsset::register($app->view);
        return true;
    }
}
