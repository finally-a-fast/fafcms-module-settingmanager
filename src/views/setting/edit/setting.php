<?php
use yii\helpers\Html;

echo $form->field($model, 'value_type')->dropDownList($model->getOptions('value_type'), [
    'placeholder' => Yii::t('fafcms-settingmanager', 'Please select'),
]);

echo $form->field($model, 'value_int')->input('number');

echo $form->field($model, 'value_varchar')->textInput(['maxlength' => true]);

echo $form->field($model, 'value_text')->textarea([
    'class' => 'materialize-textarea counter', 'maxlength' => true
]);

echo Html::tag('p', $form->field($model, 'value_bool', ['options' => ['class' => '']])->checkbox(['label' => '<span>'.$model->getAttributeLabel('value_bool').'</span>']));

echo $form->field($model, 'value_model_class')->dropDownList($model->getOptions('value_model_class'), [
    'placeholder' => Yii::t('fafcms-settingmanager', 'Please select'),
]);

echo $form->field($model, 'value_model_id')->dropDownList($model->getOptions('value_model_id'), [
    'placeholder' => Yii::t('fafcms-settingmanager', 'Please select'),
]);

echo $form->field($model, 'value_date')->textInput(['maxlength' => true]);

echo $form->field($model, 'value_time')->textInput(['maxlength' => true]);

echo $form->field($model, 'value_datetime')->textInput(['maxlength' => true]);
