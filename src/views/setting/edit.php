<?php
echo $this->render('@fafcms/fafcms/views/common/rows', [
    'rows' => [
        [
            [
                'options' => ['class' => 'col s6'],
                'content' => $this->render('@fafcms/fafcms/views/common/card', [
                    'title' => Yii::t('fafcms-settingmanager', 'Master data'),
                    'icon' => 'playlist-edit',
                    'content' => $this->render('edit/master_data', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ]),
            ],
            [
                'options' => ['class' => 'col s6'],
                'content' => $this->render('@fafcms/fafcms/views/common/card', [
                    'title' => Yii::t('fafcms-settingmanager', 'Setting'),
                    'icon' => 'settings',
                    'content' => $this->render('edit/setting', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ]),
            ]
        ],
        [
            [
                'options' => ['class' => 'col s12'],
                'content' => $this->render('@fafcms/fafcms/views/common/card', [
                    'title' => Yii::t('fafcms-settingmanager', 'Description'),
                    'icon' => 'note',
                    'content' => $this->render('edit/description', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                ]),
            ]
        ]
    ],
]);
