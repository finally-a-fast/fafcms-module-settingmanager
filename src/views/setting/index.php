<?php

use fafcms\fafcms\widgets\columns\ActionColumn;
use fafcms\fafcms\widgets\GridView;
use yii\widgets\Pjax;

Pjax::begin();

echo GridView::widget(array_merge($this->context->module->gridView, [
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        'code',
        'name',
        'value_type',
        'currentValue',
        'description:text',
        [
            'class' => ActionColumn::class,
            'visibleButtons' => [
                'view' => false,
                'update' => true,
                'delete' => true,
            ],
        ]
    ]
]));

Pjax::end();
