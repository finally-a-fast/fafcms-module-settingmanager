<?php
return [
    'sidebar_setting_group' => 'Einstellungen',
    'sidebar_setting_button' => 'Einstellungen',
    'Create {modelClass}' => '{modelClass} erstellen',
    'Save {modelClass}' => '{modelClass} speichern',
    'Error while deleting {modelClass}!' => 'Fehler beim Löschen der {modelClass}!',
    'Error while saving {modelClass}!' => 'Fehler beim Speichern der {modelClass}!',
    '{modelClass} has been saved.' => 'Die {modelClass} wurde gespeichert.',
    '{modelClass} has been deleted!' => '{modelClass} wurde gelöscht!',
    'Please check your entries and try again.' => 'Bitte prüfen Sie Ihre Eingaben und versuchen Sie es erneut.',
    'Setting' => 'Einstellung',
    'Settings' => 'Einstellungen',
    'Back' => 'Zurück',
];
