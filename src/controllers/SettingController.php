<?php
namespace fafcms\settingmanager\controllers;

use fafcms\fafcms\components\FafcmsComponent;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use fafcms\settingmanager\models\Setting;
use fafcms\settingmanager\models\SettingSearch;
use fafcms\settingmanager\Module;

/**
 * SettingController
 */
class SettingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => $this->module->accessRules['setting'],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function getIndexButtons($filterModel)
    {
        return [[
            'icon' => 'arrow-left',
            'label' => Yii::t('fafcms-settingmanager', 'Back'),
            'url' => Yii::$app->getUser()->getReturnUrl()
        ], [
            'options' => [
                'class' => 'primary'
            ],
            'icon' => 'plus',
            'label' => Yii::t('fafcms-settingmanager', 'Create {modelClass}', [
                'modelClass' => $filterModel->getEditData()['singular'],
            ]),
            'url' => ['update']
        ]];
    }

    public function actionIndex()
    {
        $filterModel  = new SettingSearch();
        $dataProvider = $filterModel->search(Yii::$app->request->queryParams);

        $this->view->params['breadcrumbs'][] = [
            'label' => $filterModel->getEditData()['plural'],
            'url' => ['index']
        ];

        $this->view->title = $filterModel->getEditData()['plural'];
        $buttons = $this->getIndexButtons($filterModel);

        Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_ACTION_BAR, $buttons);

        return $this->render('@fafcms/fafcms/views/common/rows', [
            'rows' => [[[
                'options' => ['class' => 'col s12'],
                'content' => $this->renderPartial('@fafcms/fafcms/views/common/card', [
                    'title' => $filterModel->getEditData()['plural'],
                    'icon' => $filterModel->getEditData()['icon'],
                    'content' => $this->renderPartial('index', [
                        'dataProvider' => $dataProvider,
                        'filterModel' => $filterModel,
                    ]),
                    'buttons' => $buttons
                ]),
            ]]],
        ]);
    }

    public function actionUpdate($id = null)
    {
        if ($id === null) {
            $model = new Setting();
            $model->loadDefaultValues();

            $title = Yii::t('fafcms-settingmanager', 'Create {modelClass}', [
                'modelClass' => $model->getEditData()['singular'],
            ]);
        } else {
            $model = $this->findModel($id);

            $title = Yii::t('fafcms-settingmanager', 'Update {modelClass} {modelTitle}', [
                'modelClass' => $model->getEditData()['singular'],
                'modelTitle' => $model->name
            ]);
        }

        $this->view->params['breadcrumbs'][] = [
            'label' => $model->getEditData()['plural'],
            'url' => ['index']
        ];

        $this->view->title = $title;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('fafcms-settingmanager', '{modelClass} has been saved.', [
                    'modelClass' => $model->getEditData()['singular'],
                ]));

                return $this->redirect(['index', 'id' => Yii::$app->request->get('id')]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-settingmanager', 'Error while saving {modelClass}!', [
                    'modelClass' => $model->getEditData()['singular'],
                ]).'<br><br>'.implode('<br><br>', $model->getErrorSummary(true)));
            }
        }

        $buttons = function($model, $form) {
            return [[
                'icon' => 'arrow-left',
                'label' => Yii::t('fafcms-core', 'Back'),
                'url' => ['index']
            ], [
                'form' => $form->id,
                'options' => [
                    'class' => ['primary'],
                ],
                'type' => 'submit',
                'icon' => 'content-save-outline',
                'label' =>  Yii::t('fafcms-core', 'Save'),
            ]];
        };

        return $this->render('@fafcms/fafcms/views/common/edit', [
            'model' => $model,
            'content' => function ($model, $form) use ($title) {
                return $this->renderPartial('/setting/edit', [
                    'model' => $model,
                    'form' => $form
                ]);
            },
            'buttons' => $buttons,
            'isWidget' => false
        ]);
    }

    /**
     * Deletes an existing Setting model.
     * If deletion is successful, the browser will be redirected to the previous page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('fafcms-settingmanager', '{modelClass} has been deleted!', [
                'modelClass' => $model->getEditData()['singular'],
            ]));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('fafcms-settingmanager', 'Error while deleting {modelClass}!', [
                'modelClass' => $model->getEditData()['singular'],
            ]).'<br><br>'.implode('<br><br>', $model->getErrorSummary(true)));
        }

        return $this->goBack();
    }

    /**
     * Finds a model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $modelClass
     * @return mixed the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $modelClass = null)
    {
        if ($modelClass === null) {
            $modelClass = Setting::class;
        }

        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
